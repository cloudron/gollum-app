#!/bin/bash

set -eux

if [[ ! -d "/app/data/gollum" ]]; then
    mkdir -p /app/data/gollum
    git init /app/data/gollum
fi

echo "Ensure /app/data belongs to cloudron user"
chown -R cloudron:cloudron /app/data

echo "Starting gollum"
exec /usr/local/bin/gosu cloudron:cloudron gollum --port 4567 /app/data/gollum --config /app/code/config.rb
