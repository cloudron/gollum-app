#!/usr/bin/env node

'use strict';

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    webdriver = require('selenium-webdriver');

var by = webdriver.By,
    Keys = webdriver.Key,
    until = webdriver.until;

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    var firefox = require('selenium-webdriver/chrome');
    var server, browser = new firefox.Driver();

    before(function (done) {
        var seleniumJar= require('selenium-server-standalone-jar');
        var SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
        server = new SeleniumServer(seleniumJar.path, { port: 4444 });
        server.start();

        done();
    });

    after(function (done) {
        browser.quit();
        server.stop();
        done();
    });

    var LOCATION = 'test' + Date.now();
    var TEST_STRING = 'Hello Wiki';
    var TEST_TIMEOUT = 5000;
    var app;

    xit('build app', function () {
        execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('install app', function () {
        execSync('cloudron install --new --wait --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', function () {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];

        expect(app).to.be.an('object');
    });

    it('can login', function (done) {
        browser.get('https://' + app.fqdn);
        browser.wait(until.elementLocated(by.id('inputUsername')), TEST_TIMEOUT).then(function () {
            browser.findElement(by.id('inputUsername')).sendKeys(process.env.USERNAME);
            browser.findElement(by.id('inputPassword')).sendKeys(process.env.PASSWORD);
            browser.findElement(by.id('loginForm')).submit();
            browser.wait(until.titleIs('Create a new page'), TEST_TIMEOUT).then(function () { done(); });
        });
    });

    it('can create page', function (done) {
        browser.get('https://' + app.fqdn + '/create/Home');
        browser.findElement(by.id('gollum-editor-body')).sendKeys(TEST_STRING);
        browser.findElement(by.id('gollum-editor-submit')).click();
        browser.wait(until.titleIs('Home'), TEST_TIMEOUT).then(function () { done(); });
    });

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('restore app', function () {
        execSync('cloudron restore --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('page is still there', function (done) {
        browser.get('https://' + app.fqdn + '/Home');
        browser.wait(browser.findElement(by.xpath("//*[contains(text(), '" + TEST_STRING + "')]")), TEST_TIMEOUT).then(function () { done(); });
    });

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
});
