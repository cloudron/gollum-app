# Gollum Cloudron App

This repository contains the Cloudron app package source for [Gollum](https://github.com/gollum/gollum).
To provide user authentication, Gollum wiki is used within the [omnigollum](https://github.com/arr2036/omnigollum) project for this app.

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=com.github.gollum.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id com.github.gollum.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd gollum-app
cloudron build
cloudron install
```

## TODO

* Allow configuration (use nginx + proxy_pass ?)
* Should be able to push to remote with a hook
* Take in a ssh key that will allow the user to push to the repo
* Not clear how ssh proxying should work because ssh has no SNI
* API to display the URL to push via git
* header, footer, sidebar
* allow creating custom.css and custom.js
