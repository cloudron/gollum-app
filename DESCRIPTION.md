#### Gollum is a simple wiki system built on top of Git.

This package contains omnigollum v0.1.5 and gollum v4.0.1

The app only support editing with the built-in web interface.

Gollum supports a variety of formats and extensions (Markdown, MediaWiki, Textile, ...).
On top of these formats Gollum lets you insert headers, footers, links, image, math and more.

Check out the [Gollum Wiki](https://github.com/gollum/gollum/wiki) for all of Gollum's formats and syntactic options.
