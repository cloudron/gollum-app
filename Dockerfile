FROM cloudron/base:0.9.0
MAINTAINER Johannes Zellner <johannes@cloudron.io>

EXPOSE 4567

RUN mkdir -p /app/code
WORKDIR /app/code

# https://github.com/github/pages-gem/issues/181
RUN gem install --no-ri --no-rdoc activesupport:4.2.5
RUN gem install --no-ri --no-rdoc markdown:1.2.0 github-markdown RedCloth wikicloth asciidoctor creole redcarpet org-ruby omnigollum:0.1.5 omniauth-oauth2:1.4.0 omniauth-cloudron:0.4.0

ADD start.sh config.rb /app/code/

CMD [ "/app/code/start.sh" ]
